import ifcopenshell
from pprint import pprint
from qty import *



model=ifcopenshell.open('/Users/orhunozcan/repos/DDP/DDP(Schema4).ifc')

products=model.by_type('IfcProduct')

MonetaryUnit=model.create_entity('IfcMonetaryUnit',Currency='EUR')       

def new_cost_val(valname,val):
        new_val=model.create_entity('IfcCostValue',AppliedValue=model.create_entity('IfcMeasureWithUnit',ValueComponent=model.create_entity\
                ('IfcMonetaryMeasure',val),UnitComponent=MonetaryUnit))
        return new_val

for y in products:
        if y is not None:
                if y.is_a('IfcWall'):

                        #Create Task 1 for Wall        
                        t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Brick Laying',Status='Not Complete')              
                                
                        #Create Cost Value 1 
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',15)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val1=model.create_entity('IfcCostValue',Name='Labor',AppliedValue=MeasureWithUnit)
                                
                        #Create Cost Value 2
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',20)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val2=model.create_entity('IfcCostValue',Name='Brick',AppliedValue=MeasureWithUnit)
                                
                        #Create Cost Item
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Brick Laying',CostValues=[val1,val2],CostQuantities=[qty.netvolume(y)])
                                
                        #Assign Cost Item to Task 1
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t1,RelatedObjects=[costitem])
                                
                        #Create task 2 for Wall
                        t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Plaster',Status='Not Complete')
                                
                        #Create Cost Value 1
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',10)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val1=model.create_entity('IfcCostValue',Name='Labor',AppliedValue=MeasureWithUnit)

                        #Create Cost Value 2
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',15)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val2=model.create_entity('IfcCostValue',Name='Plaster Material',AppliedValue=MeasureWithUnit)

                        #Create Cost Item for Task 2
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Plastering',CostValues=[val1,val2],\
                                CostQuantities=[qty.netarea(y)])

                        #Assign Cost Item to Task 2
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t2,RelatedObjects=[costitem])

                        #Create Task 3 for Wall
                        t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Paint',Status='Not Complete')
                                
                        #Create Cost Value 1
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',8)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val1=model.create_entity('IfcCostValue',Name='Labor',AppliedValue=MeasureWithUnit)
                                
                        #Create Cost Value 2
                        monetarymeasure=model.create_entity('IfcMonetaryMeasure',3)
                        MeasureWithUnit=model.create_entity('IfcMeasureWithUnit',ValueComponent=monetarymeasure,UnitComponent=MonetaryUnit)
                        val2=model.create_entity('IfcCostValue',Name='Paint',AppliedValue=MeasureWithUnit)
                                
                        #Create Cost Item to Task 3
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Paint',CostValues=[val1,val2],\
                                CostQuantities=[qty.netarea(y)])
                                
                        #Assign Cost Item to Task 3
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t3,RelatedObjects=[costitem])
                                
                        #Assign Tasks to Wall Element
                        model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                        
                if y.is_a('IfcColumn'):
                        #Create Task 1 for Column
                        t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Rebar',Status='Not Complete')         
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',425)
                        #Create Cost Value 2
                        val2=new_cost_val('Rebar',1500)
                        #Rebar Weight
                        rebarweight=model.create_entity("IfcQuantityWeight",Name='Weight',WeightValue=qty.netvolume(y).VolumeValue*0.10)
                        #Create Cost Item for Task 1
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Installing Rebar',CostValues=[val1,val2],\
                                CostQuantities=[rebarweight])
                        #Assign Cost Item to task 1
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t1,RelatedObjects=[costitem])     
                                
                        #Create Task 2 for Column
                        t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Formwork',Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',37.5) #1.5 hours per m2 * 25 euros per hour
                        #Create Cost Value 2
                        val2=new_cost_val('Formworks',7.5) #7.5 euros per m2
                        #Create Cost Item for Task 2
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Formwork Installation',CostValues=[val1,val2],\
                                CostQuantities=[model.create_entity('IfcQuantityArea',Name='OuterArea',AreaValue=qty.netarea(y).AreaValue/100)])
                        #Assign Cost Item to Task 2
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t2,RelatedObjects=[costitem])
                                
                        #Create Task 3 For Column
                        t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Concrete',Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',32.5) #1.3 hours per m3
                        #Create Cost Value 2
                        val2=new_cost_val('Concrete',100) #100 per m3
                        #Create Cost Item
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Concrete Pouring',CostValues=[val1,val2],\
                                CostQuantities=[qty.netvolume(y)])
                        #Assign Cost Item to Task 3
                        model.create_entity('IfcRelAssignstoControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t3,RelatedObjects=[costitem])
                                
                        #Assing Tasks to Slab
                        model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                        
                if y.is_a('IfcSlab'):
        
                        #Create Task 1 for Slab
                        t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='FormWork',Status='Not Complete')
                
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',37.5)
                        #Create Cost Value 2
                        val2=new_cost_val('Formworks',7.5)
                        #Create Cost Item for Task 1
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Formwork Installation',\
                                CostValues=[val1,val2],CostQuantities=[model.create_entity('IfcQuantityArea',Name="SideArea",\
                                        AreaValue=(qty.perimeter(y).LengthValue/100)*(qty.width(y).LengthValue/100))])
        
                        #Assign Cost Item to Task 1
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t1,RelatedObjects=[costitem])
                
                        #Create Task 2 for Slab
                        t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Rebar',Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',425) #17 mhours per tonne
                        #Create Cost Value 2
                        val2=new_cost_val('Rebar',1000) #1000 euro per tonne
                        #Rebar Weight
                        rebarweight=model.create_entity("IfcQuantityWeight",Name='Weight',WeightValue=qty.netvolume(y).VolumeValue*0.120)
                        print(qty.netvolume(y))
                        print(rebarweight)
                        #Create Cost Item for Task 2
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Rebar Installation',\
                                CostValues=[val1,val2],CostQuantities=[rebarweight])
                        #Assign Cost Item to Task 2
                        model.create_entity('IfcRelAssignsToControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t2,RelatedObjects=[costitem])

                        #Create Task 3 for Slab
                        t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Concrete',Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Labor',32.5)
                        #Create Cost Value 2
                        val2=new_cost_val('Concrete',100)
                        #Create Cost Item for Task 3
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Concrete Pouring',CostValues=[val1,val2],\
                                CostQuantities=[qty.netvolume(y)])
                        #Assign Cost Item to Task 3
                        model.create_entity('IfcRelAssignstoControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t3,RelatedObjects=[costitem])
                        
                        #Assign Tasks to Slab
                        model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                
                if y.is_a('IfcDoor'):
                        #Create Task
                        t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Door Installation',Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Door',450)
                        #Create QuantityCount Element
                        count=model.create_entity('IfcQuantityCount',Name='Count',CountValue=1)
                        #Create Cost Item for Task
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Door Intallation',CostValues=[val1],\
                                CostQuantities=[count])
                        #Assign Cost Item to Task
                        model.create_entity('IfcRelAssignstoControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t1,RelatedObjects=[costitem])
                        
                        #Assign Task to Door
                        model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1])
                if y.is_a("IfcWindow"):
                        t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Window Installation', Status='Not Complete')
                        #Create Cost Value 1
                        val1=new_cost_val('Window',250)
                        #Create QuantityCount Element
                        count=model.create_entity('IfcQuantityCount',Name='Count',CountValue=1)
                        #Create Cost Item for Task
                        costitem=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Window Intallation',CostValues=[val1],\
                                CostQuantities=[count])
                        #Assign Cost Item to Task
                        model.create_entity('IfcRelAssignstoControl',GlobalId=ifcopenshell.guid.new(),RelatingControl=t1,RelatedObjects=[costitem])
                        model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1])

costitems=model.by_type('IfcCostItem')
grandtotal=[]
for product in products:
    References=product.ReferencedBy
    for Reference in References:
        Relateds=Reference.RelatedObjects
        for Related in Relateds:
            for costitem in costitems:
                assignments2=costitem.HasAssignments
                for assignment2 in assignments2:
                    if assignment2.RelatingControl==Related:
                        for a in assignment2.RelatedObjects:
                            if a.CostQuantities[0].is_a()=='IfcQuantityVolume':                               
                                quantity=a.CostQuantities[0].VolumeValue
                                
                            if a.CostQuantities[0].is_a()=='IfcQuantityWeight':                            
                                quantity=a.CostQuantities[0].WeightValue
                                
                            if a.CostQuantities[0].is_a()=='IfcQuantityArea':                        
                                quantity=a.CostQuantities[0].AreaValue 
                                
                            if a.CostQuantities[0].is_a()=='IfcQuantityCount':                          
                                quantity=a.CostQuantities[0].CountValue
                                
                            totalval=[]
                            for x in a.CostValues:                 
                                totalval.append(x.AppliedValue.ValueComponent.wrappedValue) 
                            totalcost=sum(totalval)*quantity
                            
                            totalval.clear()
                            grandtotal.append(totalcost)

pprint(sum(grandtotal))
GrandCostVal=new_cost_val('Grand Total',sum(grandtotal))                                
GrandTotalCost=model.create_entity('IfcCostItem',GlobalId=ifcopenshell.guid.new(),Name='Grand Total',CostValues=[GrandCostVal])          


model.write('DDP(Schema4)Assigned.ifc')    


