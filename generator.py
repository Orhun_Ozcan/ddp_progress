import qrcode
import ifcopenshell
model=ifcopenshell.open('/Users/orhunozcan/repos/DDP/DDP(Schema4).ifc')
IfcSpaces=model.by_type('IfcSpace')

for IfcSpace in IfcSpaces:
    SpaceGuId=IfcSpace.GlobalId
    img = qrcode.make(SpaceGuId)
    type(img)
    if IfcSpace.LongName is not None:
        filename=IfcSpace.LongName+IfcSpace.Name+'.png'
    else: 
        filename=IfcSpace.Name+'.png'
    img.save(filename)
