
import ifcopenshell
from barread import *
from pprint import pprint
import cv2
from tkinter import *
from tkinter import ttk


model=ifcopenshell.open('/Users/orhunozcan/repos/DDP/DDP(Schema4)Assigned.ifc')
tasks=model.by_type('IfcTask')
costitems=model.by_type('IfcCostItem')

def tasklist():
    
    Tree.delete(*Tree.get_children())

    spacecode=qrcodecapture()

    relatedspaceelement=model.by_guid(spacecode)

    RelSpatialStructures=model.by_type('IfcRelSpaceBoundary')

    elements=[]

    for RelSpatialStructure in RelSpatialStructures:
        if RelSpatialStructure.RelatingSpace==relatedspaceelement:
           if RelSpatialStructure.RelatedBuildingElement not in elements:
            elements.append(RelSpatialStructure.RelatedBuildingElement)
        

    
    TaskAssigns=model.by_type('IfcRelAssignsToProduct')
    
    QueriedTasks=[]
    for assign in TaskAssigns:
        for aelement in elements:
            if aelement==assign.RelatingProduct:
                for QueriedTask in assign.RelatedObjects:
                    QueriedTasks.append([aelement.Name, QueriedTask.GlobalId,QueriedTask.Name,QueriedTask.Status,relatedspaceelement.Name])
    
    count=0

    for y in QueriedTasks:
        Tree.insert(parent='',index='end', iid=count, values=y)
        count+=1
    

def ChangeStatus():
    curItem=Tree.focus()
    itemvalues=Tree.item(curItem,'values')
    if itemvalues[3]=="Not Complete":
        Tree.item(curItem, values=(itemvalues[0],itemvalues[1],itemvalues[2],'Complete',itemvalues[4]))
        model.by_guid(itemvalues[1]).Status='Complete'
    if itemvalues[3]=="Complete":
        Tree.item(curItem, values=(itemvalues[0],itemvalues[1],itemvalues[2],'Not Complete',itemvalues[4]))
        model.by_guid(itemvalues[1]).Status='Not Complete'
    ProgressLabel.config(text="Progress Rate is : {:10.2f} %".format(progress_measure()*100))

def SaveModel():
    model.write('/Users/orhunozcan/repos/DDP/DDP(Schema4)Assigned.ifc')

def grandTotal():
    for costitem in costitems:
        if costitem.Name=='Grand Total':
            grandtotal=costitem.CostValues[0].AppliedValue.ValueComponent.wrappedValue
    return grandtotal

def progress_measure():
    

    relconts=model.by_type('IfcRelAssignsToControl')
    completedcost=[]
    for task in tasks:
        if task.Status=='Complete':
            for relcont in relconts:
                if relcont.RelatingControl==task:
                    relobj=relcont.RelatedObjects[0]
                    if relobj.is_a()=='IfcCostItem':
                        if relobj.CostQuantities[0].is_a()=='IfcQuantityVolume':                               
                            quantity=relobj.CostQuantities[0].VolumeValue
 
                        if relobj.CostQuantities[0].is_a()=='IfcQuantityWeight':                            
                            quantity=relobj.CostQuantities[0].WeightValue
   
                        if relobj.CostQuantities[0].is_a()=='IfcQuantityArea':                        
                            quantity=relobj.CostQuantities[0].AreaValue 
                   
                        if relobj.CostQuantities[0].is_a()=='IfcQuantityCount':                          
                            quantity=relobj.CostQuantities[0].CountValue
                       
                        totalval=[]
                        for x in relobj.CostValues:                 
                            totalval.append(x.AppliedValue.ValueComponent.wrappedValue) 
                        totalcost=sum(totalval)*quantity
                        completedcost.append(totalcost)
    progressrate=sum(completedcost)/grandTotal()
    return progressrate


root=Tk()
root.geometry('800x400')
root.title('Task List')

frm=ttk.Frame(root, padding=10)

Task_Label=ttk.Label(root, text='Tasks').pack()
Tree=ttk.Treeview()
ProgressLabel=ttk.Label(root,text="Progress Rate is : {:10.2f} %".format(progress_measure()*100))



Tree['columns']=('Element Name','TaskID','Task Name', 'Status','Space')
Tree.pack(pady=10)

#Format Columns
Tree.column('#0', width=0)
Tree.column('Element Name', anchor=W,width=250)
Tree.column('TaskID',anchor=W,width=200)
Tree.column('Task Name', anchor=W,width=120)
Tree.column('Status', anchor=CENTER,width=120)
Tree.column('Space',anchor=CENTER,width=80)
#Create Headers
Tree.heading('Element Name', text='Element Name',anchor=W)
Tree.heading('TaskID',text='Task ID',anchor=W)
Tree.heading('Task Name',text='Task Name')
Tree.heading('Status',text='Status',anchor=W)
Tree.heading('Space',text='Space No',anchor=CENTER)


Lb1=Listbox(frm)
#Buttons

ttk.Button(root, text='Scan QR-Code', command=tasklist).pack()
ttk.Button(root, text='Change Status',command=ChangeStatus).pack()
ttk.Button(root, text='Save Model', command=SaveModel).pack()
ProgressLabel.pack(pady=25)
#ttk.Button(root,text='Progress Rate',command=progress_measure).pack(pady=10)
#ProgressLabel = Label(root,text=progress_measure()).pack()

root.mainloop()