import cv2

def qrcodecapture():
    cap= cv2.VideoCapture(0)

    detector=cv2.QRCodeDetector()

    while True:
        _, img=cap.read()
        data, bbox, _ =detector.detectAndDecode(img)
        if data:
            a=data
            break
    
        cv2.imshow("QRCODEScanner", img)
        if cv2.waitKey(1)==ord("q"):
            a=None
            break
    
    cap.release()
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    
    return a 
if __name__== '__main__':
    qrcodecapture()

    